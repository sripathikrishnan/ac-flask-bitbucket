from ac_flask.bitbucket import Addon, room_client, sender
from flask import Flask

addon = Addon(app=Flask(__name__),
              key="ac-flask-bitbucket-greeter",
              name="Bitbucket Hello",
              allow_room=True,
              scopes=['send_notification'])


if __name__ == '__main__':
    addon.run()